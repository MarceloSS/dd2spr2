import React from "react";

import PageOne from './Components/PageOne';
import PageTwo from './Components/PageTwo';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div>
          <Link to="/">Page1</Link>
          <Link to="/PageTwo">Page2</Link>
        </div>
        <switch>
          <ul>
            <li>
              <Route exact path ="/"><PageOne/></Route>
            </li>
            <li>
              <Route path="/PageTwo"><PageTwo/></Route>
            </li>
          </ul>
        </switch>
      </header>
    </div>
  );
}

export default App;
